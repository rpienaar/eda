## Synopsis

Erlang Data Adaptor.

Setup data forwarding from a list of supported protocols.
Data is forwarded to a configured module.

The list of available incomming protocols are:

* TCP/IP V4
* TCP/IP v6 ( TODO )
* UDP over v4
* UDP over v6 ( TODO )
* SSL over v4 ( TODO )
* SSL over v6 ( TODO )
* HTTP ( TODO )
* HTTPS (TODO)
* WebSocket ( TODO )
* More comming

## Code Example

Show what the library does as concisely as possible,
developers should be able to figure out **how** your project solves their problem by looking at the code example.
Make sure the API you are showing off is obvious, and that your code is short and concise.

## Motivation

A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

## Installation

Provide code examples and explanations of how to get the project.

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README.
For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

Let people know how they can dive into the project, include important links to things like issue trackers,
irc, twitter accounts if applicable.

## License

A short snippet describing the license (MIT, Apache, etc.)

TODO:
- move examples to seperate folder